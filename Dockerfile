FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build

WORKDIR /app
COPY . .

RUN dotnet restore

RUN dotnet build

RUN dotnet publish -c Release -o /publish/
# end of build

FROM mcr.microsoft.com/dotnet/core/runtime:3.1

COPY --from=build /publish/Lancache.CacheManager /usr/local/bin/

VOLUME /data/cache

ENTRYPOINT ["/usr/local/bin/Lancache.CacheManager"]