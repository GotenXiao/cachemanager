﻿using System;
using System.IO;
using System.Text;

namespace Lancache.CacheManager
{
    class Program
    {
        private const string KeyPrefix = "\nKEY: ";
        
        private static byte[] GetUtf8Bytes(string s) => Encoding.UTF8.GetBytes(s);

        private static void WriteUsage()
        {
            Console.Error.WriteLine("<CDN provider name> <request URL>");
            Console.Error.WriteLine("e.g. steam /depot/887861/chunk/0e0433f196ac17a8423a6c9c1f2a22faf0e0115e");
        }
        
        public static int Main(string[] args)
        {
            if (args.Length < 2)
            {
                WriteUsage();
                return -1;
            }

            string cdnProvider = args[0];
            string searchUrl = args[1];
            
            Span<byte> prefixBytes = GetUtf8Bytes(KeyPrefix).AsSpan();
            
            Span<byte> cdnProviderSpan = GetUtf8Bytes(cdnProvider).AsSpan();
            Span<byte> searchUrlSpan = GetUtf8Bytes(searchUrl).AsSpan();
            
            var spaceSpan = new ReadOnlySpan<byte>(new byte[] { 0x20 });
            
            Console.WriteLine("[{0:O}] Starting search for {1} {2}", DateTime.Now, cdnProvider, searchUrl);
            
            var files = Directory.GetFiles("/data/cache", "*", SearchOption.AllDirectories);

            foreach (var path in files)
            {
                using var file = File.Open(
                    path,
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.ReadWrite | FileShare.Delete
                );

                // Read the first 1KB of the file
                const int bufferSize = 1024;
                var buffer = new byte[bufferSize];
                int bytesRead = file.Read(buffer, 0, bufferSize);
                if (bytesRead == 0)
                {
                    // No data, skip file
                    continue;
                }

                var bufferSpan = buffer.AsSpan();
                int startPos = bufferSpan.IndexOf(prefixBytes);
                if (startPos < 0)
                {
                    // No match for prefix, skip file
                    continue;
                }

                // Advance the span window to the end of the prefix
                bufferSpan = bufferSpan.Slice(startPos + prefixBytes.Length);
                
                if (
                    !bufferSpan
                        .Slice(0, cdnProviderSpan.Length)
                        .SequenceEqual(cdnProviderSpan)
                )
                {
                    // Does not match CDN provider, skip file
                    continue;
                }
                
                // Advance the span window to the end of the CDN provider string
                bufferSpan = bufferSpan.Slice(cdnProvider.Length);

                if (bufferSpan.Slice(0, searchUrlSpan.Length).SequenceEqual(searchUrlSpan))
                {
                    // Candidate file
                    Console.WriteLine("[{0:O}] Found {1}", DateTime.Now, path);
                }
            }
            
            Console.WriteLine("[{0:O}] Finished search for {1} {2}", DateTime.Now, cdnProvider, searchUrl);
            
            return 0;
        }
    }
}